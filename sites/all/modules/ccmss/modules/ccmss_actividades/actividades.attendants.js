(function ($) {
  Drupal.behaviors.ccmssActividadesAttendants = {
    attach: function (context, settings) {
      this.bindCountFields(context, settings);
      this.disableCountFields(context, settings);
    },
    bindCountFields: function(context, settings) {
      // Bind the Inline Entity Form and the sin registro fields to the
      // refreshCounts function
      
      var fieldSelectors = settings.ccmss.attendant_count_field_selectors;
      
      $(fieldSelectors['mujeres-sin-registro'] + ', ' + fieldSelectors['hombres-sin-registro'], context).once('refreshcounts', function() {
        var $item = $(this);
        $item.change(function() {
          Drupal.behaviors.ccmssActividadesAttendants.refreshCounts();
        })
      });

      var ccmssActividadesAttendants = this;
      if(typeof(ccmssActividadesAttendants.ajaxEvents) == 'undefined') {
        $(document).ajaxSuccess(ccmssActividadesAttendants.iefChange);
        ccmssActividadesAttendants.ajaxEvents = true;
      }
      Drupal.behaviors.ccmssActividadesAttendants.refreshCounts();

    },
    disableCountFields: function(context, settings) {
      var fieldSelectors = settings.ccmss.attendant_count_field_selectors;
      $(fieldSelectors['mujeres-total']).attr('readonly', 'readonly');
      $(fieldSelectors['hombres-total']).attr('readonly', 'readonly');
      $(fieldSelectors['total']).attr('readonly', 'readonly');

      // $('.node-asistencia_tecnica-form, .node-taller-form', context).once('disablecountfields', function() {
      //   var $item = $(this);
      //   
      //   $item.submit(function(evt) {
      //     console.log('The form was submitted');
      //     console.log($(this));
      //     console.log(evt);
      //     evt.preventDefault();
      //     $(fieldSelectors['mujeres-total']).attr('disabled', '');
      //     $(fieldSelectors['hombres-total']).attr('disabled', '');
      //     $(fieldSelectors['total']).attr('disabled', '');
      //   });
      // });
    },
    refreshCounts: function () {
      // Get text field values for hombres y mujeres sin registro
      var fieldSelectors = Drupal.settings.ccmss.attendant_count_field_selectors;
      var hombresSinRegistro = $(fieldSelectors['hombres-sin-registro']).val();
      var mujeresSinRegistro = $(fieldSelectors['mujeres-sin-registro']).val();

      hombresSinRegistro = (hombresSinRegistro == '') ? 0 : parseInt(hombresSinRegistro);
      mujeresSinRegistro = (mujeresSinRegistro == '') ? 0 : parseInt(mujeresSinRegistro);

      hombresSinRegistro = (isNaN(hombresSinRegistro)) ? 0 : hombresSinRegistro;
      mujeresSinRegistro = (isNaN(mujeresSinRegistro)) ? 0 : mujeresSinRegistro;

      // Get table rows and extract attendant counts for hombres and mujeres
      var $iefTableCells = $(fieldSelectors['sexo-cell']);
      var mujeres = 0;
      var hombres = 0;

      $iefTableCells.each(function() {
        var $cell = $(this);
        if($cell.text() == 'Femenino') {
          mujeres++;
        }
        else if($cell.text() == 'Masculino') {
          hombres++;
        }
      });

      var mujeresTotal = mujeresSinRegistro + mujeres;
      var hombresTotal = hombresSinRegistro + hombres;
      var total = mujeresTotal + hombresTotal;

      $(fieldSelectors['mujeres-total']).val(mujeresTotal);
      $(fieldSelectors['hombres-total']).val(hombresTotal);
      $(fieldSelectors['total']).val(total);

    },
    iefChange: function(event, xhr, settings) {
      if(typeof(settings.extraData) == 'undefined') {
        return;
      }
      var triggeringElement = settings.extraData._triggering_element_name; 

      var addedNewNode = /ief-add-submit-[a-zA-Z0-9]*/.test(triggeringElement);
      var removedNode = /ief-remove-confirm-[a-zA-Z0-9]*-[0-9]*/.test(triggeringElement);
      var addedExistingNode = /ief-reference-submit-[a-zA-Z0-9]*/.test(triggeringElement);

      if(addedNewNode || removedNode || addedExistingNode) {
        Drupal.behaviors.ccmssActividadesAttendants.refreshCounts();
      }
    }
  };
}(jQuery));
