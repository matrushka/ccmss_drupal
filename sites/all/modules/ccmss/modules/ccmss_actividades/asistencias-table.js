(function($) {
  $(function() {
    $controls_container = $('.table-controls-wrapper');
    $table = $('.view-actividades.view-display-id-panel_pane_1 .view-content table.views-table');

    $toggle_full = $controls_container.find('.toggle-fullview');

    if($table.hasClass('show-fullview')) {
      $toggle_full.addClass('on');
    }
    else {
      $toggle_full.removeClass('on');
    }

    $toggle_full.click(function(e) {
      e.preventDefault();
      $table.toggleClass('show-fullview');

      if($table.hasClass('show-fullview')) {
        $(this).addClass('on');
      }
      else {
        $(this).removeClass('on');
      }
    });
  });
})(jQuery);
