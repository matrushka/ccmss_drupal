<?php

function ccmss_actividades_calculate_age($nacimiento) {
  $birth_year = $nacimiento[0]['value'];
  $current_year = date('Y');
  $age = (int)$current_year - (int)$birth_year;

  return $age;
}

function _responsive_bartik_menu_menu_actividades_block_visibility() {
  $alias_components = explode('/', drupal_get_path_alias());
  if($alias_components[0] == 'actividades') {
    return true;
  }

  // Whenever the user is visiting a actividades-related path
  if(arg(0) == 'actividades') {
    return true;
  }

  // when the user is editing a taller or asistencia técnica
  if(arg(0) == 'node' && arg(2) == 'edit') {
    $node = node_load(arg(1));
    if($node->type == 'taller' || $node->type == 'asistencia_tecnica') {
      return true;
    }
  }

  // when a user is adding talleres or asistencias técnicas
  if(arg(0) == 'node' && arg(1) == 'add' && (arg(2) == 'taller' || arg(2) == 'asistencia-tecnica')) {
    return true;
  }
}

// Alias to make it work with the CCMSS theme
function _ccmss_theme_menu_menu_actividades_block_visibility() {
  return _responsive_bartik_menu_menu_actividades_block_visibility();
}
