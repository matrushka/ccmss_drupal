(function ($) {
  Drupal.behaviors.ccmssActividadesCosts = {
    attach: function (context, settings) {
      this.bindCostFields(context, settings);
      this.disableTotalField(context, settings);
    },
    bindCostFields: function(context, settings) {
      //Bind the cost fields to the refreshCosts function
      $(this.fieldSelectors.join()).once('refreshcosts', function() {
        var $item = $(this);
        $item.change(function() {
          Drupal.behaviors.ccmssActividadesCosts.refreshCosts();
        })
      });

      Drupal.behaviors.ccmssActividadesCosts.refreshCosts();

    },
    disableTotalField: function(context, settings) {
      $('#edit-field-taller-costos-und-0-value').attr('readonly', 'readonly');
    },
    refreshCosts: function () {
      var totalCost = 0;
      $(this.fieldSelectors.join()).each(function() {
        var toAdd = Number($(this).val());
        if(!isNaN(toAdd)) {
          totalCost += toAdd;
          $(this).removeClass('error');
        }
        else {
          $(this).addClass('error');
        }
      });

      $('#edit-field-taller-costos-und-0-value').val(totalCost);
    },
    fieldSelectors: ['#edit-field-taller-costos-alimentacion-und-0-value', '#edit-field-taller-costos-tallerista-und-0-value', '#edit-field-taller-costos-viaje-und-0-value']
  };
}(jQuery));
