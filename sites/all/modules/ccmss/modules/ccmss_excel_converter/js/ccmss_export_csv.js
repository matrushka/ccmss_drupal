(function($) {
  $(function() {
    // var $tables = $('table.csv-export');
    var tables = [
      {container: '.hoja-de-tiempo', buttonContainer: '.hoja-de-tiempo-header', table: 'table', download: 'hoja-de-tiempo.csv'},
      {container: '.view-tiempo.view-display-id-panel_pane_1 .detail-table-container', buttonContainer: '.export-button', table: 'table.sticky-enabled', download: 'horas.csv'},
      {container: '.view-tiempo.view-display-id-panel_pane_1 .summary-table-container', buttonContainer: '.export-button', table: 'table.sticky-enabled', download: 'resumen-horas-y-porcentajes.csv'},
      {container: '.view-tiempo.view-display-id-panel_pane_2', buttonContainer: '.view-filters', table: 'table', download: 'dias-no-laborados.csv'},
      {container: '.view-actividades.view-display-id-panel_pane_1', buttonContainer: '.view-filters', table: 'table', download: 'asistencias-tecnicas.csv'},
      {container: '.view-actividades.view-display-id-panel_pane_2', buttonContainer: '.view-filters', table: 'table', download: 'talleres.csv'},
      {container: '.view-actividades.view-display-id-panel_pane_3', buttonContainer: '.view-filters', table: 'table', download: 'participantes.csv'},
      {container: '.view-actividades.view-display-id-panel_pane_4', buttonContainer: '.view-filters', table: 'table', download: 'asistencias-por-participante.csv'},
      {container: '.view-actividades.view-display-id-panel_pane_5', buttonContainer: '.view-filters', table: 'table', download: 'talleres-por-participante.csv'}
    ];
    
    // Add the export button to tables with the csv-export class
    $.each(tables, function() {
      var $container = $(this.container);
      var $buttonContainer = $container.find(this.buttonContainer);
      $buttonContainer.append('<a href="'+getExportHref(this)+'" download="'+this.download+'">Descargar en formato CSV</a>');
    })

    function getExportHref(tableConfig) {
      var $container = $(tableConfig.container);
      var $table = $container.find(tableConfig.table);

      var tableArray = htmlTableToArray($table);
      var csvString = tableArrayToCSV(tableArray);
      
      var href = "data:text/csv;charset=utf-8,%EF%BB%BF" + encodeURI(csvString);
      return href;
    }

    function htmlTableToArray($table) {
      var table = [];
      var $rows = $table.find('tr');
      var skip;
      var rowPosition = 0;

      var skipCells = [];

      $rows.each(function() {
        var $row = $(this);
        var $cells = $row.find('td, th');
        var cellPosition = 0;
        var nextSkipCells = [];

        table.push([]);
        var currentRow = table[rowPosition];

        $cells.each(function() {
          var $cell = $(this);
          var rowspan = $cell.attr('rowspan');
          var colspan = $cell.attr('colspan');
          
          if(typeof(rowspan) != 'undefined') {
            nextSkipCells[cellPosition] = (typeof(colspan) == 'undefined') ? 1 : colspan;
          }

          currentRow.push($cell.text());
          cellPosition++;

          if(typeof(colspan) != 'undefined') {
            for(var i=1; i < colspan; i++) {
              currentRow.push('');
              cellPosition++;
            }
          }

        });
        
        // Add the skipeed cells from last row's rowspans
        // This is done at the end to ensure that even cells that come after
        // the each() loop are inserted
        for(var i=0; i < skipCells.length; i++) {
          var skip = skipCells[i];
          if(typeof(skip) != 'undefined') {
            currentRow.splice(i, 0, '');
          }
        }

        // move nextSkipCells to skipCells
        skipCells = nextSkipCells.slice();

        rowPosition++;
      })

      return table;
    }

    function tableArrayToCSV(tableArray) {
      var rows = [];

      for(var i = 0; i < tableArray.length; i++) {
        // Get rid of double quotes and newline characters that would break the CSV
        for(var j = 0; j < tableArray[i].length; j++) {
          tableArray[i][j] = tableArray[i][j].replace(/"/g, '');
          tableArray[i][j] = tableArray[i][j].replace(/\n/g, '');
        }
        rows.push('"' + tableArray[i].join('","') + '"');
      }

      return rows.join("\n");
    }
  })
})(jQuery)

var tableToExcel = (function() {
  var uri = 'data:application/vnd.ms-excel;base64,'
    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
  return function(table, name) {
    if (!table.nodeType) table = document.getElementById(table)
    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
    window.location.href = uri + base64(format(template, ctx))
  }
})()
