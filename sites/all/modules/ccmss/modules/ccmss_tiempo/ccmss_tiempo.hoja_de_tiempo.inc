<?php

// Composer autoload, needed for QueryPath
$module_path = drupal_get_path('module', 'ccmss_tiempo');
require($module_path . '/vendor/autoload.php');
use Masterminds\HTML5;

/**
 * Hoja de tiempo page callback
 * 
 * @param target_user
 * Fully loaded user object
 * 
 * @param start
 * start date in Y-m-d format
 *
 * @param end
 * end date in Y-m-d format
 */
function ccmss_tiempo_hoja_de_tiempo($target_user, $start, $end) {
  global $user;
  
  // Don't allow the user to generate a hoja de tiempo for another user
  // if she doesn't have permission to
  if($user->uid != $target_user->uid && !user_access('ver hojas de tiempo')) {
    drupal_access_denied();
    return;
  }

  $actividades = ccmss_tiempo_hoja_de_tiempo_get_actividades();
  $tiempos = ccmss_tiempo_hoja_de_tiempo_get_tiempos($target_user, $start, $end);
  $dias_no_laborados = ccmss_tiempo_hoja_de_tiempo_get_dnl($target_user, $start, $end);
  $days = ccmss_tiempo_get_days_in_range($start, $end);
  $months = ccmss_tiempo_get_months_in_range($start, $end);

  $output = theme('hoja_de_tiempo', array(
    'user' => $target_user, 
    'start' => $start, 
    'end' => $end, 
    'actividades' => $actividades, 
    'tiempos' => $tiempos, 
    'dias_no_laborados' => $dias_no_laborados,
    'days' => $days, 
    'months' => $months
  ));

  // Render the hoja de tiempo table
  return $output;
}

/*
 * Get the horas data for a selected user within a given period
 *
 * @param target_user
 * Drupal user object
 * 
 * @param start
 * Start date in Y-m-d format
 * 
 * @param end
 * End date in Y-m-d format
 *
 */
function ccmss_tiempo_hoja_de_tiempo_get_tiempos($target_user, $start, $end) {
  // Get the time data
  $query = <<<EOQ
  SELECT
    CONCAT(DATE_FORMAT(fecha.field_tiempo_fecha_value, '%Y-%m-%d'), '+', actividad.field_tiempo_actividad_tid) AS id, 
    DATE_FORMAT(fecha.field_tiempo_fecha_value, '%Y-%m-%d') AS fecha, 
    SUM(horas.field_tiempo_horas_value) AS horas, 
    actividad.field_tiempo_actividad_tid AS tid

    FROM drupal_node node
      LEFT JOIN {field_data_field_tiempo_fecha} fecha ON fecha.entity_id = node.nid
      LEFT JOIN {field_data_field_tiempo_actividad} actividad ON actividad.entity_id = node.nid
      LEFT JOIN {field_data_field_tiempo_horas} horas ON horas.entity_id = node.nid
      LEFT JOIN {field_data_field_tiempo_persona} persona ON persona.entity_id = node.nid
    
    WHERE 
      node.type = 'tiempo' AND
      persona.field_tiempo_persona_target_id = :uid AND
      fecha.field_tiempo_fecha_value >= :start_date AND
      fecha.field_tiempo_fecha_value <= :end_date
    
    GROUP BY id,fecha.field_tiempo_fecha_value,actividad.field_tiempo_actividad_tid
    ORDER BY fecha.field_tiempo_fecha_value ASC
EOQ;

  $result = db_query($query, array(
    ':uid' => $target_user->uid,
    ':start_date' => $start,
    ':end_date' => $end
  ));

  $tiempos = $result->fetchAllAssoc('id');
  return $tiempos;
}

/**
 * Generates an array of días no laborados for a specific user and range of dates
 * 
 * @param target_user
 * Drupal user
 *
 * @param start_date
 * Range start date in Y-m-d format
 * 
 * @param end_date
 * Range end date in y-m-d format
 */
function ccmss_tiempo_hoja_de_tiempo_get_dnl($target_user, $start_date, $end_date) {
  // Get the días no laborados data
  $query = <<<EOQ
  SELECT DATE_FORMAT(fecha.field_dnl_fecha_value, '%Y-%m-%d') AS start, DATE_FORMAT(fecha.field_dnl_fecha_value2, '%Y-%m-%d') AS end
    FROM drupal_node node
    LEFT JOIN {field_data_field_dnl_fecha} fecha ON fecha.entity_id = node.nid
    LEFT JOIN {field_data_field_tiempo_persona} persona ON persona.entity_id = node.nid
    
    WHERE node.type = 'dia_no_laborado' AND
    persona.field_tiempo_persona_target_id = :uid AND
    fecha.field_dnl_fecha_value >= :start_date AND
    fecha.field_dnl_fecha_value <= :end_date AND
    node.status = 1
EOQ;
  
  $result = db_query($query, array(
    ':uid' => $target_user->uid,
    ':start_date' => $start_date,
    ':end_date' => $end_date
  ));

  $dnl = $result->fetchAll();

  // Generate days for the ranges found
  $days = array();
  foreach($dnl as $range) {
    $days_in_range = ccmss_tiempo_get_days_in_range($range->start, $range->end);
    $days = array_merge($days, $days_in_range);
  }

  return array_unique($days);
}

/*
 * Get the actividades taxonomy terms
 */
function ccmss_tiempo_hoja_de_tiempo_get_actividades() {
  // Get the actividades tree
  $vocabulary = taxonomy_vocabulary_machine_name_load('actividades');
  $actividades = taxonomy_get_tree($vocabulary->vid);

  return $actividades;
}

/*
 * Get the days that fall within of a given range to be able to iterate over them
 *
 * @param start
 * Start date in Y-m-d format
 * 
 * @param end
 * End date in Y-m-d format
 */
function ccmss_tiempo_get_days_in_range($start, $end) {
  $start_date = new DateTime($start);
  $end_date = new DateTime($end);
  $end_date = $end_date->modify('+1 day'); // include the end date
  $interval = new DateInterval('P1D');

  $days = new DatePeriod($start_date, $interval, $end_date);

  $formatted_days = array();
  foreach($days as $day) {
    $formatted_days[] = $day->format('Y-m-d');
  }

  return $formatted_days;
}

/*
 * Get the months that fall within of a given range to be able to iterate over them
 *
 * @param start
 * Start date in Y-m-d format
 * 
 * @param end
 * End date in Y-m-d format
 */
function ccmss_tiempo_get_months_in_range($start, $end) {
  $start_date = new DateTime($start);
  $end_date = new DateTime($end);
  $end_date = $end_date->modify('+1 day'); // include the end date
  $interval = new DateInterval('P1M');

  $months = new DatePeriod($start_date, $interval, $end_date);
  
  $formatted_months = array();
  foreach($months as $month) {
    $timestamp = $month->format('U');
    $month_string = format_date($timestamp, 'custom', 'F');
    $month_index = format_date($timestamp, 'custom', 'm');
    $formatted_months[] = array('name' => $month_string, 'number' => $month_index);
  }

  return $formatted_months;
  
}

/**
 * Get the sum of hours for a day from a tiempos array
 *
 * @param day
 * Day in format Y-m-d
 *
 * @param tiempos
 * Tiempos array of objects as generated by ccmss_tiempo_hoja_de_tiempo_get_tiempos
 */
function ccmss_tiempo_get_day_sum($day, $tiempos) {
  $sum = 0;
  foreach($tiempos as $tiempo) {
    if($tiempo->fecha == $day) {
      $sum += $tiempo->horas;
    }
  }
  if($sum > 0) {
    return $sum;
  }
  else {
    return '';
  }
}

/**
 * Get the sum of hours for an actividad from a tiempos array
 *
 * @param actividades
 * Actividades terms
 *
 * @param tidActividad
 * Actividad tid
 *
 * @param tiempos
 * Tiempos array
 */
function ccmss_tiempo_get_actividad_sum($actividades, $tidActividad, $tiempos, $recursive=TRUE) {
  $sum = 0;
  $childrenSum = 0;

  if($recursive) {
    // Extract children TIDs to sum their times to the parent time
    $tid = $tidActividad;
    $getChildren = function($term) use ($tid) {
      if(in_array($tid, $term->parents)) {
        return $term->tid;
      }
      else {
        return FALSE;
      }
    };
    
    $childrenIds = array_filter(array_map($getChildren, $actividades));
  }

  // Get sum for this term
  foreach($tiempos as $tiempo) {
    if($tiempo->tid == $tidActividad) {
      $sum += $tiempo->horas;
    }
    
    if($recursive && in_array($tiempo->tid, $childrenIds)) {
      $childrenSum += $tiempo->horas;
    }
  }
  
  if($recursive) {
    $sum = $sum + $childrenSum;
  }

  if($sum > 0) {
    return $sum;
  }
  else {
    return '';
  }
}

/**
 * Get the total sum of hours from a tiempos array
 *
 * @param tiempos
 * Tiempos array of objects as generated by ccmss_tiempo_hoja_de_tiempo_get_tiempos
 */ 
function ccmss_tiempo_get_total_sum($tiempos) {
  $sum = 0;
  foreach($tiempos as $tiempo) {
    $sum += $tiempo->horas;
  }
  return $sum;
}

/**
 * Check if a particular day is a holiday
 *
 * @param day
 * Day in Y-m-d- format
 */
function ccmss_tiempo_is_holiday($day) {
  global $ccmss_holidays_list;
  // Is this a Saturday or Sunday?
  $time = strtotime($day);
  $week_day = date('l', $time);
  if($week_day == 'Saturday' || $week_day == 'Sunday') {
    return true;
  }

  if(!isset($ccmss_holidays_list)) {
    $holidays_config = variable_get('ccmss_tiempo_holidays', '');
    $ccmss_holidays_list = explode("\r\n", $holidays_config);
  }

  if(in_array($day, $ccmss_holidays_list)) {
    return true;
  }
  else {
    return false;
  }
}

/**
 * This function builds the hoja de tiempo table in HTML format
 */
function _ccmss_render_hoja_de_tiempo_table($user, $start, $end, $actividades, $tiempos, $dias_no_laborados, $days, $months) {
  // Preprocess data
  $num_actividades = count($actividades);

  // Initialize table output. This template works very different to almost all other templates.
  // The output is built with DOM functions since a linear building would be too complex.
  // The table data is then used in the page output.
  $html5 = new HTML5();
  $table_start = <<<TABLE
<table id="hoja-de-tiempo" class="csv-export">
  <thead>
    <tr class="ht-header-1">
      <th class="actividad" rowspan="2">Proyecto > actividad</th>
    </tr>
    <tr class="ht-header-2"></tr>
  </thead>
  <tbody>
  </tbody>
</table>
TABLE;
  $output = $html5->loadHTML($table_start);

  // Add the actividad rows and header (left) cells
  foreach($actividades as $actividad) {
    qp($output, 'tbody')->append("<tr class='actividad-$actividad->tid depth-$actividad->depth'><td>$actividad->name</td></tr>");
  }

  // Add the summary row at the bottom
  qp($output, 'tbody')->append("<tr class='summary-row'><td>Total</td></tr>");

  // Add day header cells and count days for each month
  $month_counts = array();
  foreach($days as $day) {
    $components = explode('-', $day);
    $month = $components[1];
    $day_number = $components[2];

    $dnl_class = '';
    if(in_array($day, $dias_no_laborados)) {
      $dnl_class = 'no-laborado';
    }

    if(!array_key_exists($month, $month_counts)) {
      $month_counts[$month] = 0;
    }
    $month_counts[$month]++;

    qp($output, '.ht-header-2')->append("<th class='day day-$day $dnl_class'>$day_number</th>");
  }

  // Add month header cells
  foreach($months as $month) {
    $month_number = $month['number'];
    $month_name = $month['name'];
    $month_count = $month_counts[$month['number']];
    qp($output, '.ht-header-1')->append("<th class='month month-$month_number' colspan='$month_count'>$month_name</th>");
  }

  // Add totals column
  qp($output, '.ht-header-1')->append("<th class='summary-column' rowspan='2'>Total</th>");
  qp($output, '.ht-header-1')->append("<th class='summary-column' rowspan='2'>Porcentaje</th>");

  // Add time to cells
  foreach($actividades as $actividad) {
    foreach($days as $day) {
      $cell_class = 'actividad-' . $actividad->tid  . '_dia-' . $day . ' time-cell';
      $cell_style = '';
      $horas = '';

      if(ccmss_tiempo_is_holiday($day)) {
        $cell_class .= ' is-holiday';
        $horas = 'Día festivo';
      }

      if(in_array($day, $dias_no_laborados)) {
        $cell_class .= ' no-laborado';
        $horas = 'Día no laborado';
      }

      $key = $day . '+' . $actividad->tid;
      if(array_key_exists($key, $tiempos)) {
        $horas = $tiempos[$key]->horas;
        $cell_class .= ' has-time';
      }

      qp($output, ".actividad-$actividad->tid")->append("<td class='$cell_class' $cell_style>$horas</td>");
    }
  }

  // Calculate total in advance to be able to get percentages
  // and later output the total
  $total = ccmss_tiempo_get_total_sum($tiempos);

  // Add by-actividad summaries
  foreach($actividades as $actividad) {
    $tidActividad = $actividad->tid;
    $sum = ccmss_tiempo_get_actividad_sum($actividades, $tidActividad, $tiempos);
    qp($output, ".actividad-" . $actividad->tid)->append("<td class='sum sum-actividad-$actividad->tid'>$sum</td>");

    // Calculate percent but avoid division by zero warnings
    if($total == 0) {
      $percent = 0;
    }
    else {
      $percent = round($sum / $total * 100, 2);
    }

    qp($output, ".actividad-" . $actividad->tid)->append("<td class='sum percent-actividad-$actividad->tid'>$percent%</td>");
    unset($sum);
  }

  // Add by-day summaries
  foreach($days as $day) {
    $cell_class = "sum sum-day-$day";
    $cell_style = '';
    if(ccmss_tiempo_is_holiday($day)) {
      $cell_class .= ' is-holiday';
    }
    if(in_array($day, $dias_no_laborados)) {
      $cell_class .= ' no-laborado';
    }

    $sum = ccmss_tiempo_get_day_sum($day, $tiempos);
    qp($output, ".summary-row")->append("<td class='$cell_class' $cell_style>$sum</td>");
    unset($sum);
  }

  // Add total
  qp($output, ".summary-row")->append("<td class='sum-totals'>$total</td>");
  qp($output, ".summary-row")->append("<td class='sum-totals'>100%</td>");

  // Print the generated table
  return $output->saveHTML();
}
