<?php

/**
 * Generates the hoja de tiempo form array
 * 
 * @param form
 * the form array
 * 
 * @param form_state
 * the form state array
 */
function ccmss_tiempo_formulario_hoja_de_tiempo($form, &$form_state) {
  $form = ccmss_tiempo_formulario_usuario_periodo();
  return $form;
}

/**
 * Generates the horas form array
 * 
 * @param form
 * the form array
 * 
 * @param form_state
 * the form state array
 */
function ccmss_tiempo_formulario_horas($form, &$form_state) {
  $form = ccmss_tiempo_formulario_usuario_periodo();

  $actividad_terms = taxonomy_get_tree(2);

  $actividad_select_options = ['' => t('- Any -')];
  foreach($actividad_terms as $term) {
    $actividad_select_options[$term->tid] = ($term->depth > 0) ? '-' . $term->name : $term->name;
  }

  // Add Actividad taxonomy term select
  $form['actividad'] = array(
    '#title' => 'Actividad',
    '#type' => 'select',
    '#options' => $actividad_select_options,
    '#weight' => 3
  );

  $form['submit_button']['#weight'] = 10;

  return $form;
}

/**
 * Generates the días no laborados form array
 * 
 * @param form
 * the form array
 * 
 * @param form_state
 * the form state array
 */
function ccmss_tiempo_formulario_dnl($form, &$form_state) {
  $form = ccmss_tiempo_formulario_usuario_periodo();
  return $form;
}

/**
 * Generates a generic form array for selecting a user and a date range
 * This array can later be modified to suit particular needs
 */
function ccmss_tiempo_formulario_usuario_periodo() {
  global $user;

  $form['usuario'] = array(
    '#type' => 'textfield',
    '#title' => t('Persona'),
    '#size' => 30,
    '#maxlength' => 60,
    '#autocomplete_path' => 'user/autocomplete',
    '#default_value' => $user->name,
    '#weight' => -1,
  );

  $date_format = 'd/m/Y';
  $default_date = date($date_format);

  $form['inicio'] = array(
    '#type' => t( 'date_popup' ),
    '#title' => 'Desde',
    '#date_format' => $date_format,
    '#default_value' => $default_date,
    '#date_year_range' => '-3:+0',
    '#date_label_position' => 'none',
    '#required' => TRUE
  );

  $form['fin'] = array(
    '#type' => t( 'date_popup' ),
    '#title' => 'Hasta',
    '#date_format' => $date_format,
    '#default_value' => $default_date,
    '#date_year_range' => '-3:+0',
    '#date_label_position' => 'none',
    '#required' => TRUE
  );

  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Generar'),
  );

  return $form;
}

function ccmss_tiempo_formulario_hoja_de_tiempo_submit($form, &$form_state) {
  $user_name = $form_state['values']['usuario'];
  $target_user = user_load_by_name($user_name);

  if(empty( $user_name ) || $target_user == FALSE) {
    form_set_error('usuario', 'El usuario seleccionado no existe');
    return;
  }

  $uid = $target_user->uid;
  $start_date = $form_state['values']['inicio'];
  $end_date = $form_state['values']['fin'];

  drupal_goto("tiempo/hoja-de-tiempo/$uid/$start_date/$end_date");
}

function ccmss_tiempo_formulario_dnl_submit($form, &$form_state) {
  $options = array(
    'report_path' => 'tiempo/dias-no-laborados',
    'date_start_field' => 'field_dnl_fecha_value',
    'date_end_field' => 'field_dnl_fecha_value2',
  );
  
  ccmss_tiempo_formulario_report_submit($options, $form_state);
}

function ccmss_tiempo_formulario_horas_submit($form, &$form_state) {
  $options = array(
    'report_path' => 'tiempo/horas',
    'date_start_field' => 'field_tiempo_fecha_value',
    'date_end_field' => 'field_tiempo_fecha_value_1',
    'actividad_field' => 'term_node_tid_depth',
  );
  
  ccmss_tiempo_formulario_report_submit($options, $form_state);
}

/**
 * Submit handler for the horas and días no laborados forms
 * 
 * @param options
 * Array with options to handle the report request:
 * — report_path      the path of the panels variant that serves the report, for example tiempo/horas
 * — date_start_field  the name of the start date field
 * - date_end_field    the name of the end date field
 *
 * @param form_state
 * The current form state as received by the submit handler
 */
function ccmss_tiempo_formulario_report_submit($options, &$form_state) {
  if(empty( $options['report_path'] ) || empty( $options['date_start_field'] ) || empty( $options['date_end_field'] )) {
    form_set_error('', 'Missing options in form submit handler');
  }
  else {
    $report_path = $options['report_path'];
    $start_field = $options['date_start_field'];
    $end_field = $options['date_end_field'];
  }

  $user_name = $form_state['values']['usuario'];
  $target_user = user_load_by_name($user_name);

  if(empty( $user_name ) || $target_user == FALSE) {
    form_set_error('usuario', 'El usuario seleccionado no existe');
    return;
  }

  $uid = $target_user->uid;
  $start_date = $form_state['values']['inicio'];
  $end_date = $form_state['values']['fin'];

  $start_date = date('d/m/Y', strtotime($start_date));
  $end_date = date('d/m/Y', strtotime($end_date));

  $query = array(
    $start_field . '[value][date]' => $start_date,
    $end_field . '[value][date]' => $end_date,
  );

  // SPECIAL CASES
  // If an actividad parameter was received, add it (only for horas reports)
  if(isset($options['actividad_field']) && $form_state['values']['actividad']) {
    $actividad_field = $options['actividad_field'];
    $actividad_tid = $form_state['values']['actividad'];
    $query[$actividad_field] = $actividad_tid;
  }

  drupal_goto("$report_path/$uid", array(
    'query' => $query
  ));
}

