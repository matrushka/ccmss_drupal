(function($) {
  $(function() {
    $summary_container = $('.summary-table-container');
    $summary_table = $summary_container.find('table.summary-table');

    $empty_toggle = $summary_container.find('.toggle-empty');
    $activities_toggle = $summary_container.find('.toggle-activities');

    if($summary_table.hasClass('show-activities')) {
      $activities_toggle.addClass('on');
    }
    else {
      $(this).removeClass('on');
    }

    if($summary_table.hasClass('show-empty')) {
      $empty_toggle.addClass('on');
    }
    else {
      $(this).removeClass('on');
    }

    $empty_toggle.click(function(e) {
      e.preventDefault();
      $summary_table.toggleClass('show-empty-hours');

      if($summary_table.hasClass('show-empty-hours')) {
        $(this).addClass('on');
      }
      else {
        $(this).removeClass('on');
      }
    });

    $activities_toggle.click(function(e) {
      e.preventDefault();
      $summary_table.toggleClass('show-activities');

      if($summary_table.hasClass('show-activities')) {
        $(this).addClass('on');
      }
      else {
        $(this).removeClass('on');
      }
    });



  });
})(jQuery);
