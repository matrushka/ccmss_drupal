<?php
function _ccmss_tiempo_title($node) {
  if(empty($node->field_tiempo_persona['und'])) {
    global $user;
    $username = $user->name;
  }
  else {
    $subject_id = $node->field_tiempo_persona['und']['0']['target_id'];
    $user = user_load($subject_id);
    $username = $user->name;
  }

  $date = $node->field_tiempo_fecha['und'][0]['value'];
  $date = substr($date, 0, 10);
  
  return "Tiempo de $username el $date";
}

function _ccmss_dnl_title($node) {
  if(empty($node->field_tiempo_persona['und'])) {
    global $user;
    $username = $user->name;
  }
  else {
    $subject_id = $node->field_tiempo_persona['und']['0']['target_id'];
    $user = user_load($subject_id);
    $username = $user->name;
  }

  $date = $node->field_dnl_fecha['und'][0]['value'];
  $date = substr($date, 0, 10);

  $date_end = $node->field_dnl_fecha['und'][0]['value2'];
  $date_end = substr($date_end, 0, 10);

  if($date == $date_end) {
    return "Día no laborado de $username en $date";
  }
  else {
    return "Día(s) no laborado(s) de $username del $date al $date_end";
  }
}

function _responsive_bartik_menu_menu_tiempo_block_visibility() {
  $alias_components = explode('/', drupal_get_path_alias());
  if($alias_components[0] == 'tiempo') {
    return true;
  }

  // Whenever the user is visiting a tiempo-related path
  if(arg(0) == 'tiempo') {
    return true;
  }
  
  // when the user is editing a tiempo or dia no laborado
  if(arg(0) == 'node' && arg(2) == 'edit') {
    $node = node_load(arg(1));
    if($node->type == 'tiempo' || $node->type == 'dia_no_laborado') {
      return true;
    }
  }
  
  // when a user is adding tiempo or día no laborado
  if(arg(0) == 'node' && arg(1) == 'add' && (arg(2) == 'tiempo' || arg(2) == 'dia-no-laborado')) {
    return true;
  }
  
  // When the actividades taxonomy tree is being managed
  if(arg(2) == 'taxonomy' && arg(3) == 'actividades') {
    return true;
  }
}

function _ccmss_theme_menu_menu_tiempo_block_visibility() {
  return _responsive_bartik_menu_menu_tiempo_block_visibility();
}

// Builds a table with a summary of hours devoted to projects
// and activities, to display with Mis Horas and Registros de horas
function ccmss_tiempo_hours_summary() {
  global $user;
  $params = drupal_get_query_parameters();

  $vocabulary = taxonomy_vocabulary_machine_name_load('actividades');
  $actividades_vid = $vocabulary->vid;


  // If a parent term was received, insert it at the beginning of the terms array
  if(array_key_exists('term_node_tid_depth', $params) && is_numeric($params['term_node_tid_depth'])) {
    $parent_tid = $params['term_node_tid_depth'];
    $parent = taxonomy_term_load($parent_tid);
    $parent->parents = array();
    $show_activities = true;

    $activities = taxonomy_get_tree($actividades_vid, $parent_tid);
    array_unshift($activities, $parent);
  }
  else {
    $activities = taxonomy_get_tree($actividades_vid);
    $show_activities = false;
  }

  // Get all tiempo records for the selected period
  $uid = arg(2);
  $uid = (!empty($uid)) ? $uid : $user->uid; 
  $report_user = user_load($uid);
  
  if(array_key_exists('field_tiempo_fecha_value', $params) && !empty($params['field_tiempo_fecha_value']['value']['date'])) {
    $start_components = explode('/', $params['field_tiempo_fecha_value']['value']['date']);
    $start = $start_components[2] . '-' . $start_components[1] . '-' . $start_components[0];
  }
  else {
    $start = '2000-01-01';
  }

  if(array_key_exists('field_tiempo_fecha_value_1', $params) && !empty($params['field_tiempo_fecha_value_1']['value']['date'])) {
    $end_components = explode('/', $params['field_tiempo_fecha_value_1']['value']['date']);
    $end = $end_components[2] . '-' . $end_components[1] . '-' . $end_components[0];
  }
  else {
    $end = date('Y-m-d', time() + 86400);
  }
  $tiempos = ccmss_tiempo_hoja_de_tiempo_get_tiempos($report_user, $start, $end);
  $horas_total = ccmss_tiempo_get_total_sum($tiempos);
  
  // Get sums and percentages by activity to build the summary table
  $table = [];
  foreach($activities as $activity) {
    $table[$activity->tid] = array(
      "name" => $activity->name,
      "tid" => $activity->tid,
      "hours" => ccmss_tiempo_get_actividad_sum($activities, $activity->tid, $tiempos),
      "is_child" => count($activity->parents) > 0 && (!count($activity->parents) != 1 && $activity->parents[0] != '0'),
    );
    $table[$activity->tid]["percentage"] = ($table[$activity->tid]['hours'] == 0) ? '' : round($table[$activity->tid]["hours"] / $horas_total * 100, 2) . '%';
  }
  
  $header = array(
    t('Actividad o proyecto'),
    t('Horas'),
    t('Porcentaje')
  );
  $rows = array();
  foreach($table as $row) {
    $class = array();
    $class[] = ($row['is_child']) ? 'activity-row' : 'project-row';
    $class[] = (empty($row['hours'])) ? 'empty-hours' : 'has-hours';
    
    $rows[] = array(
      'data' => array(
        $row['name'],
        $row['hours'],
        $row['percentage']
      ),
       'class' => $class,
    );
  }

  $table_render_array['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => ($show_activities) ? array('class' => array('show-activities', 'summary-table')) : array('class' => array('summary-table')),
  );

  $rendered_table = drupal_render($table_render_array);
  $output = theme('tiempo_summary', array(
    'rendered_table' => $rendered_table,
    'toggle_activities' => true,
    'toggle_empty' => true
  ));

  // Add JS for toggling row visibility
  $path = drupal_get_path('module', 'ccmss_tiempo');
  drupal_add_js($path . '/js/tiempo-summary.js');

  return $output;
}
