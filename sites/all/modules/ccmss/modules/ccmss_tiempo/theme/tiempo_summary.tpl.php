<div class="summary-table-container">

<div class="table-controls">
  <?php if($toggle_activities): ?>
  <a href="#" class="toggle-button toggle-activities"><span class="led"></span><span class="text">Actividades</span></a>
  <?php endif; ?>

  <?php if($toggle_empty): ?>
  <a href="#" class="toggle-button toggle-empty"><span class="led"></span><span class="text">Filas vacías</span></a>
  <?php endif; ?>
</div>

<?php
print $rendered_table;
?>
</div>
