<?php
/**
 * @file
 * feature_ccmss_base.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function feature_ccmss_base_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'color_responsive_bartik_palette';
  $strongarm->value = array(
    'top' => '#FFFFFF',
    'bottom' => '#dddddd',
    'bg' => '#ffffff',
    'sidebar' => '#f6f6f2',
    'sidebarborders' => '#f9f9f9',
    'footer' => '#292929',
    'titleslogan' => '#fffeff',
    'text' => '#3b3b3b',
    'link' => '#779E49',
  );
  $export['color_responsive_bartik_palette'] = $strongarm;

  return $export;
}
