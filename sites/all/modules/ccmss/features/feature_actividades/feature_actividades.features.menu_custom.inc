<?php
/**
 * @file
 * feature_actividades.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function feature_actividades_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-actividades.
  $menus['menu-actividades'] = array(
    'menu_name' => 'menu-actividades',
    'title' => 'Actividades',
    'description' => 'Menú para la herramienta de registro de actividades (asistencias técnicas y talleres).',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Actividades');
  t('Menú para la herramienta de registro de actividades (asistencias técnicas y talleres).');

  return $menus;
}
