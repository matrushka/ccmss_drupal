<?php
/**
 * @file
 * feature_actividades.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function feature_actividades_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_at_asistentes_set|node|asistencia_tecnica|form';
  $field_group->group_name = 'group_at_asistentes_set';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'asistencia_tecnica';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Asistentes',
    'weight' => '12',
    'children' => array(
      0 => 'field_at_asistentes',
      1 => 'field_at_hombres_sin_registro',
      2 => 'field_at_mujeres_sin_registro',
      3 => 'field_at_mujeres_total',
      4 => 'field_at_hombres_total',
      5 => 'field_at_total',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Asistentes',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-at-asistentes-set field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_at_asistentes_set|node|asistencia_tecnica|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_at_registro_tiempo|node|asistencia_tecnica|form';
  $field_group->group_name = 'group_at_registro_tiempo';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'asistencia_tecnica';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Registro de tiempo',
    'weight' => '11',
    'children' => array(
      0 => 'field_actividad_auto_registro',
      1 => 'field_actividad_usuarios',
      2 => 'field_actividad_duracion',
      3 => 'field_actividad_actividad',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-at-registro-tiempo field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_at_registro_tiempo|node|asistencia_tecnica|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_taller_asistentes_set|node|taller|form';
  $field_group->group_name = 'group_taller_asistentes_set';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'taller';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Asistentes',
    'weight' => '12',
    'children' => array(
      0 => 'field_taller_asistentes',
      1 => 'field_taller_hombres',
      2 => 'field_taller_mujeres',
      3 => 'field_taller_mujeres_total',
      4 => 'field_taller_hombres_total',
      5 => 'field_taller_total',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-taller-asistentes-set field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_taller_asistentes_set|node|taller|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_taller_costos|node|taller|form';
  $field_group->group_name = 'group_taller_costos';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'taller';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Costos',
    'weight' => '10',
    'children' => array(
      0 => 'field_taller_costos_alimentacion',
      1 => 'field_taller_costos_tallerista',
      2 => 'field_taller_costos',
      3 => 'field_taller_costos_viaje',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-taller-costos field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_taller_costos|node|taller|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_taller_registro_tiempo|node|taller|form';
  $field_group->group_name = 'group_taller_registro_tiempo';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'taller';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Registro de tiempo',
    'weight' => '11',
    'children' => array(
      0 => 'field_actividad_duracion',
      1 => 'field_actividad_usuarios',
      2 => 'field_actividad_auto_registro',
      3 => 'field_actividad_actividad',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-taller-registro-tiempo field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_taller_registro_tiempo|node|taller|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Asistentes');
  t('Costos');
  t('Registro de tiempo');

  return $field_groups;
}
