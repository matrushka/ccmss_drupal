<?php
/**
 * @file
 * feature_tiempo.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function feature_tiempo_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-tiempo.
  $menus['menu-tiempo'] = array(
    'menu_name' => 'menu-tiempo',
    'title' => 'Tiempo',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Tiempo');

  return $menus;
}
