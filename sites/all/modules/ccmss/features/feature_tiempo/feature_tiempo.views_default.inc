<?php
/**
 * @file
 * feature_tiempo.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function feature_tiempo_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'tiempo';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Tiempo';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'table';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'No se encontraron registros de tiempo.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_tiempo_persona_target_id']['id'] = 'field_tiempo_persona_target_id';
  $handler->display->display_options['relationships']['field_tiempo_persona_target_id']['table'] = 'field_data_field_tiempo_persona';
  $handler->display->display_options['relationships']['field_tiempo_persona_target_id']['field'] = 'field_tiempo_persona_target_id';
  $handler->display->display_options['relationships']['field_tiempo_persona_target_id']['label'] = 'Persona';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'field_tiempo_persona_target_id';
  $handler->display->display_options['fields']['name']['label'] = 'Persona';
  /* Field: Content: Actividad */
  $handler->display->display_options['fields']['field_tiempo_actividad']['id'] = 'field_tiempo_actividad';
  $handler->display->display_options['fields']['field_tiempo_actividad']['table'] = 'field_data_field_tiempo_actividad';
  $handler->display->display_options['fields']['field_tiempo_actividad']['field'] = 'field_tiempo_actividad';
  /* Contextual filter: User: Uid */
  $handler->display->display_options['arguments']['uid']['id'] = 'uid';
  $handler->display->display_options['arguments']['uid']['table'] = 'users';
  $handler->display->display_options['arguments']['uid']['field'] = 'uid';
  $handler->display->display_options['arguments']['uid']['relationship'] = 'field_tiempo_persona_target_id';
  $handler->display->display_options['arguments']['uid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['uid']['exception']['title'] = 'Todo(s)';
  $handler->display->display_options['arguments']['uid']['default_argument_type'] = 'current_user';
  $handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Has taxonomy terms (with depth) */
  $handler->display->display_options['filters']['term_node_tid_depth']['id'] = 'term_node_tid_depth';
  $handler->display->display_options['filters']['term_node_tid_depth']['table'] = 'node';
  $handler->display->display_options['filters']['term_node_tid_depth']['field'] = 'term_node_tid_depth';
  $handler->display->display_options['filters']['term_node_tid_depth']['exposed'] = TRUE;
  $handler->display->display_options['filters']['term_node_tid_depth']['expose']['operator_id'] = 'term_node_tid_depth_op';
  $handler->display->display_options['filters']['term_node_tid_depth']['expose']['label'] = 'Tiene términos de categorización(con profundidad)';
  $handler->display->display_options['filters']['term_node_tid_depth']['expose']['operator'] = 'term_node_tid_depth_op';
  $handler->display->display_options['filters']['term_node_tid_depth']['expose']['identifier'] = 'term_node_tid_depth';
  $handler->display->display_options['filters']['term_node_tid_depth']['type'] = 'select';
  $handler->display->display_options['filters']['term_node_tid_depth']['vocabulary'] = 'actividades';
  $handler->display->display_options['filters']['term_node_tid_depth']['hierarchy'] = 1;
  $handler->display->display_options['filters']['term_node_tid_depth']['depth'] = '0';

  /* Display: Registros de tiempo de usuario */
  $handler = $view->new_display('panel_pane', 'Registros de tiempo de usuario', 'panel_pane_1');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Mis registros de horas';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'uid' => 'uid',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'uid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['sticky'] = TRUE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['header'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Fecha */
  $handler->display->display_options['fields']['field_tiempo_fecha']['id'] = 'field_tiempo_fecha';
  $handler->display->display_options['fields']['field_tiempo_fecha']['table'] = 'field_data_field_tiempo_fecha';
  $handler->display->display_options['fields']['field_tiempo_fecha']['field'] = 'field_tiempo_fecha';
  $handler->display->display_options['fields']['field_tiempo_fecha']['settings'] = array(
    'format_type' => 'compacto',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'field_tiempo_persona_target_id';
  $handler->display->display_options['fields']['name']['label'] = 'Nombre';
  $handler->display->display_options['fields']['name']['link_to_user'] = FALSE;
  /* Field: Content: Actividad */
  $handler->display->display_options['fields']['field_tiempo_actividad']['id'] = 'field_tiempo_actividad';
  $handler->display->display_options['fields']['field_tiempo_actividad']['table'] = 'field_data_field_tiempo_actividad';
  $handler->display->display_options['fields']['field_tiempo_actividad']['field'] = 'field_tiempo_actividad';
  $handler->display->display_options['fields']['field_tiempo_actividad']['type'] = 'hs_taxonomy_term_reference_hierarchical_text';
  /* Field: Content: Horas */
  $handler->display->display_options['fields']['field_tiempo_horas']['id'] = 'field_tiempo_horas';
  $handler->display->display_options['fields']['field_tiempo_horas']['table'] = 'field_data_field_tiempo_horas';
  $handler->display->display_options['fields']['field_tiempo_horas']['field'] = 'field_tiempo_horas';
  $handler->display->display_options['fields']['field_tiempo_horas']['settings'] = array(
    'thousand_separator' => '',
    'decimal_separator' => '.',
    'scale' => '2',
    'prefix_suffix' => 1,
  );
  /* Field: Content: Detalles */
  $handler->display->display_options['fields']['field_tiempo_detalles']['id'] = 'field_tiempo_detalles';
  $handler->display->display_options['fields']['field_tiempo_detalles']['table'] = 'field_data_field_tiempo_detalles';
  $handler->display->display_options['fields']['field_tiempo_detalles']['field'] = 'field_tiempo_detalles';
  /* Field: Content: Edit link */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['label'] = 'Editar';
  $handler->display->display_options['defaults']['sorts'] = FALSE;
  /* Sort criterion: Content: Fecha (field_tiempo_fecha) */
  $handler->display->display_options['sorts']['field_tiempo_fecha_value']['id'] = 'field_tiempo_fecha_value';
  $handler->display->display_options['sorts']['field_tiempo_fecha_value']['table'] = 'field_data_field_tiempo_fecha';
  $handler->display->display_options['sorts']['field_tiempo_fecha_value']['field'] = 'field_tiempo_fecha_value';
  $handler->display->display_options['sorts']['field_tiempo_fecha_value']['expose']['label'] = 'Fecha (field_tiempo_fecha)';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: User: Uid */
  $handler->display->display_options['arguments']['uid']['id'] = 'uid';
  $handler->display->display_options['arguments']['uid']['table'] = 'users';
  $handler->display->display_options['arguments']['uid']['field'] = 'uid';
  $handler->display->display_options['arguments']['uid']['relationship'] = 'field_tiempo_persona_target_id';
  $handler->display->display_options['arguments']['uid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['uid']['exception']['title'] = 'Todo(s)';
  $handler->display->display_options['arguments']['uid']['title_enable'] = TRUE;
  $handler->display->display_options['arguments']['uid']['title'] = 'Registros de horas de %1';
  $handler->display->display_options['arguments']['uid']['default_argument_type'] = 'current_user';
  $handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'tiempo' => 'tiempo',
  );
  /* Filter criterion: Desde */
  $handler->display->display_options['filters']['field_tiempo_fecha_value']['id'] = 'field_tiempo_fecha_value';
  $handler->display->display_options['filters']['field_tiempo_fecha_value']['table'] = 'field_data_field_tiempo_fecha';
  $handler->display->display_options['filters']['field_tiempo_fecha_value']['field'] = 'field_tiempo_fecha_value';
  $handler->display->display_options['filters']['field_tiempo_fecha_value']['ui_name'] = 'Desde';
  $handler->display->display_options['filters']['field_tiempo_fecha_value']['operator'] = '>=';
  $handler->display->display_options['filters']['field_tiempo_fecha_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_tiempo_fecha_value']['expose']['operator_id'] = 'field_tiempo_fecha_value_op';
  $handler->display->display_options['filters']['field_tiempo_fecha_value']['expose']['label'] = 'Desde';
  $handler->display->display_options['filters']['field_tiempo_fecha_value']['expose']['operator'] = 'field_tiempo_fecha_value_op';
  $handler->display->display_options['filters']['field_tiempo_fecha_value']['expose']['identifier'] = 'field_tiempo_fecha_value';
  $handler->display->display_options['filters']['field_tiempo_fecha_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  $handler->display->display_options['filters']['field_tiempo_fecha_value']['form_type'] = 'date_popup';
  $handler->display->display_options['filters']['field_tiempo_fecha_value']['year_range'] = '-3:+0';
  /* Filter criterion: Hasta */
  $handler->display->display_options['filters']['field_tiempo_fecha_value_1']['id'] = 'field_tiempo_fecha_value_1';
  $handler->display->display_options['filters']['field_tiempo_fecha_value_1']['table'] = 'field_data_field_tiempo_fecha';
  $handler->display->display_options['filters']['field_tiempo_fecha_value_1']['field'] = 'field_tiempo_fecha_value';
  $handler->display->display_options['filters']['field_tiempo_fecha_value_1']['ui_name'] = 'Hasta';
  $handler->display->display_options['filters']['field_tiempo_fecha_value_1']['operator'] = '<=';
  $handler->display->display_options['filters']['field_tiempo_fecha_value_1']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_tiempo_fecha_value_1']['expose']['operator_id'] = 'field_tiempo_fecha_value_1_op';
  $handler->display->display_options['filters']['field_tiempo_fecha_value_1']['expose']['label'] = 'Hasta';
  $handler->display->display_options['filters']['field_tiempo_fecha_value_1']['expose']['operator'] = 'field_tiempo_fecha_value_1_op';
  $handler->display->display_options['filters']['field_tiempo_fecha_value_1']['expose']['identifier'] = 'field_tiempo_fecha_value_1';
  $handler->display->display_options['filters']['field_tiempo_fecha_value_1']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  $handler->display->display_options['filters']['field_tiempo_fecha_value_1']['form_type'] = 'date_popup';
  $handler->display->display_options['filters']['field_tiempo_fecha_value_1']['year_range'] = '-3:+0';
  /* Filter criterion: Content: Has taxonomy terms (with depth) */
  $handler->display->display_options['filters']['term_node_tid_depth']['id'] = 'term_node_tid_depth';
  $handler->display->display_options['filters']['term_node_tid_depth']['table'] = 'node';
  $handler->display->display_options['filters']['term_node_tid_depth']['field'] = 'term_node_tid_depth';
  $handler->display->display_options['filters']['term_node_tid_depth']['exposed'] = TRUE;
  $handler->display->display_options['filters']['term_node_tid_depth']['expose']['operator_id'] = 'term_node_tid_depth_op';
  $handler->display->display_options['filters']['term_node_tid_depth']['expose']['label'] = 'Actividad';
  $handler->display->display_options['filters']['term_node_tid_depth']['expose']['operator'] = 'term_node_tid_depth_op';
  $handler->display->display_options['filters']['term_node_tid_depth']['expose']['identifier'] = 'term_node_tid_depth';
  $handler->display->display_options['filters']['term_node_tid_depth']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
  );
  $handler->display->display_options['filters']['term_node_tid_depth']['type'] = 'select';
  $handler->display->display_options['filters']['term_node_tid_depth']['vocabulary'] = 'actividades';
  $handler->display->display_options['filters']['term_node_tid_depth']['hierarchy'] = 1;
  $handler->display->display_options['filters']['term_node_tid_depth']['depth'] = '1';
  $handler->display->display_options['allow']['use_pager'] = 'use_pager';
  $handler->display->display_options['allow']['items_per_page'] = 'items_per_page';
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 'more_link';
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 0;
  $handler->display->display_options['allow']['exposed_form'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 0;
  $handler->display->display_options['argument_input'] = array(
    'uid' => array(
      'type' => 'panel',
      'context' => 'entity:file.original',
      'context_optional' => 0,
      'panel' => '1',
      'fixed' => '1',
      'label' => 'Usuario: Uid',
    ),
  );
  $handler->display->display_options['inherit_panels_path'] = '1';

  /* Display: Registros de días no laborados */
  $handler = $view->new_display('panel_pane', 'Registros de días no laborados', 'panel_pane_2');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Mis registros de días no laborados';
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'uid' => 'uid',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'uid' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['sticky'] = TRUE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'field_tiempo_persona_target_id';
  $handler->display->display_options['fields']['name']['label'] = 'Nombre';
  $handler->display->display_options['fields']['name']['link_to_user'] = FALSE;
  /* Field: Content: Día(s) */
  $handler->display->display_options['fields']['field_dnl_fecha']['id'] = 'field_dnl_fecha';
  $handler->display->display_options['fields']['field_dnl_fecha']['table'] = 'field_data_field_dnl_fecha';
  $handler->display->display_options['fields']['field_dnl_fecha']['field'] = 'field_dnl_fecha';
  $handler->display->display_options['fields']['field_dnl_fecha']['settings'] = array(
    'format_type' => 'compacto',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_remaining_days' => 0,
  );
  /* Field: Content: Razón */
  $handler->display->display_options['fields']['field_dnl_razon']['id'] = 'field_dnl_razon';
  $handler->display->display_options['fields']['field_dnl_razon']['table'] = 'field_data_field_dnl_razon';
  $handler->display->display_options['fields']['field_dnl_razon']['field'] = 'field_dnl_razon';
  /* Field: Content: Detalles */
  $handler->display->display_options['fields']['field_dnl_detalles']['id'] = 'field_dnl_detalles';
  $handler->display->display_options['fields']['field_dnl_detalles']['table'] = 'field_data_field_dnl_detalles';
  $handler->display->display_options['fields']['field_dnl_detalles']['field'] = 'field_dnl_detalles';
  /* Field: Content: Edit link */
  $handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
  $handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
  $handler->display->display_options['fields']['edit_node']['label'] = 'Editar';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: User: Uid */
  $handler->display->display_options['arguments']['uid']['id'] = 'uid';
  $handler->display->display_options['arguments']['uid']['table'] = 'users';
  $handler->display->display_options['arguments']['uid']['field'] = 'uid';
  $handler->display->display_options['arguments']['uid']['relationship'] = 'field_tiempo_persona_target_id';
  $handler->display->display_options['arguments']['uid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['uid']['exception']['title'] = 'Todo(s)';
  $handler->display->display_options['arguments']['uid']['title_enable'] = TRUE;
  $handler->display->display_options['arguments']['uid']['title'] = 'Días no laborados de %1';
  $handler->display->display_options['arguments']['uid']['default_argument_type'] = 'current_user';
  $handler->display->display_options['arguments']['uid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['uid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['uid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'dia_no_laborado' => 'dia_no_laborado',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Desde */
  $handler->display->display_options['filters']['field_dnl_fecha_value']['id'] = 'field_dnl_fecha_value';
  $handler->display->display_options['filters']['field_dnl_fecha_value']['table'] = 'field_data_field_dnl_fecha';
  $handler->display->display_options['filters']['field_dnl_fecha_value']['field'] = 'field_dnl_fecha_value';
  $handler->display->display_options['filters']['field_dnl_fecha_value']['ui_name'] = 'Desde';
  $handler->display->display_options['filters']['field_dnl_fecha_value']['operator'] = '>=';
  $handler->display->display_options['filters']['field_dnl_fecha_value']['group'] = 1;
  $handler->display->display_options['filters']['field_dnl_fecha_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_dnl_fecha_value']['expose']['operator_id'] = 'field_dnl_fecha_value_op';
  $handler->display->display_options['filters']['field_dnl_fecha_value']['expose']['label'] = 'Desde';
  $handler->display->display_options['filters']['field_dnl_fecha_value']['expose']['operator'] = 'field_dnl_fecha_value_op';
  $handler->display->display_options['filters']['field_dnl_fecha_value']['expose']['identifier'] = 'field_dnl_fecha_value';
  $handler->display->display_options['filters']['field_dnl_fecha_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  $handler->display->display_options['filters']['field_dnl_fecha_value']['form_type'] = 'date_popup';
  $handler->display->display_options['filters']['field_dnl_fecha_value']['year_range'] = '-3:+0';
  /* Filter criterion: Hasta */
  $handler->display->display_options['filters']['field_dnl_fecha_value2']['id'] = 'field_dnl_fecha_value2';
  $handler->display->display_options['filters']['field_dnl_fecha_value2']['table'] = 'field_data_field_dnl_fecha';
  $handler->display->display_options['filters']['field_dnl_fecha_value2']['field'] = 'field_dnl_fecha_value2';
  $handler->display->display_options['filters']['field_dnl_fecha_value2']['ui_name'] = 'Hasta';
  $handler->display->display_options['filters']['field_dnl_fecha_value2']['operator'] = '<=';
  $handler->display->display_options['filters']['field_dnl_fecha_value2']['group'] = 1;
  $handler->display->display_options['filters']['field_dnl_fecha_value2']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_dnl_fecha_value2']['expose']['operator_id'] = 'field_dnl_fecha_value2_op';
  $handler->display->display_options['filters']['field_dnl_fecha_value2']['expose']['label'] = 'Hasta';
  $handler->display->display_options['filters']['field_dnl_fecha_value2']['expose']['operator'] = 'field_dnl_fecha_value2_op';
  $handler->display->display_options['filters']['field_dnl_fecha_value2']['expose']['identifier'] = 'field_dnl_fecha_value2';
  $handler->display->display_options['filters']['field_dnl_fecha_value2']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  $handler->display->display_options['filters']['field_dnl_fecha_value2']['form_type'] = 'date_popup';
  $handler->display->display_options['filters']['field_dnl_fecha_value2']['year_range'] = '-3:+0';
  $handler->display->display_options['allow']['use_pager'] = 'use_pager';
  $handler->display->display_options['allow']['items_per_page'] = 'items_per_page';
  $handler->display->display_options['allow']['offset'] = 0;
  $handler->display->display_options['allow']['link_to_view'] = 0;
  $handler->display->display_options['allow']['more_link'] = 'more_link';
  $handler->display->display_options['allow']['path_override'] = 0;
  $handler->display->display_options['allow']['title_override'] = 0;
  $handler->display->display_options['allow']['exposed_form'] = 0;
  $handler->display->display_options['allow']['fields_override'] = 0;
  $handler->display->display_options['argument_input'] = array(
    'uid' => array(
      'type' => 'panel',
      'context' => 'entity:file.original',
      'context_optional' => 0,
      'panel' => '1',
      'fixed' => '1',
      'label' => 'Usuario: Uid',
    ),
  );
  $handler->display->display_options['inherit_panels_path'] = '1';
  $translatables['tiempo'] = array(
    t('Master'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('No se encontraron registros de tiempo.'),
    t('Persona'),
    t('Actividad'),
    t('Todo(s)'),
    t('Tiene términos de categorización(con profundidad)'),
    t('Registros de tiempo de usuario'),
    t('Mis registros de horas'),
    t('Fecha'),
    t('Nombre'),
    t('Horas'),
    t('Detalles'),
    t('Editar'),
    t('Fecha (field_tiempo_fecha)'),
    t('Registros de horas de %1'),
    t('Desde'),
    t('Hasta'),
    t('View panes'),
    t('Registros de días no laborados'),
    t('Mis registros de días no laborados'),
    t('Día(s)'),
    t('Razón'),
    t('Días no laborados de %1'),
  );
  $export['tiempo'] = $view;

  return $export;
}
