<?php
/**
 * @file
 * feature_tiempo.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function feature_tiempo_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_dnl_usuario|node|dia_no_laborado|form';
  $field_group->group_name = 'group_dnl_usuario';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'dia_no_laborado';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Selección de usuario',
    'weight' => '9',
    'children' => array(
      0 => 'field_tiempo_persona',
      1 => 'field_tiempo_otra_persona',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Selección de usuario',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-dnl-usuario field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_dnl_usuario|node|dia_no_laborado|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_tiempo_usuario|node|tiempo|form';
  $field_group->group_name = 'group_tiempo_usuario';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'tiempo';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Selección de usuario',
    'weight' => '6',
    'children' => array(
      0 => 'field_tiempo_persona',
      1 => 'field_tiempo_otra_persona',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Selección de usuario',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-tiempo-usuario field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_tiempo_usuario|node|tiempo|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Selección de usuario');

  return $field_groups;
}
