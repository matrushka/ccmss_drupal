<?php
/**
 * @file
 * feature_tiempo.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feature_tiempo_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function feature_tiempo_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function feature_tiempo_node_info() {
  $items = array(
    'dia_no_laborado' => array(
      'name' => t('Día no laborado'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'tiempo' => array(
      'name' => t('Tiempo'),
      'base' => 'node_content',
      'description' => t('Registro de tiempo de trabajo dedicado a una sola actividad, por parte de una persona.'),
      'has_title' => '1',
      'title_label' => t('Título'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
