<?php
/**
 * @file
 * feature_tiempo.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function feature_tiempo_taxonomy_default_vocabularies() {
  return array(
    'actividades' => array(
      'name' => 'Actividades',
      'machine_name' => 'actividades',
      'description' => 'Árbol de proyectos y actividades para el registro de tiempos',
      'hierarchy' => 1,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
