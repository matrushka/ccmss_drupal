<?php
/**
 * @file
 * feature_tiempo.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function feature_tiempo_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_edit__tiempo-registrar-d-a-no-laborado-2';
  $handler->task = 'node_edit';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -27;
  $handler->conf = array(
    'title' => 'Tiempo: registrar día no laborado - sin menú tiempo',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'tiempo-registrar-d-a-no-laborado-2',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'dia_no_laborado' => 'dia_no_laborado',
            ),
          ),
          'context' => 'argument_node_edit_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'panels_breadcrumbs_state' => 1,
    'panels_breadcrumbs_titles' => 'Tiempo
Registrar día no laborado',
    'panels_breadcrumbs_paths' => 'tiempo',
    'panels_breadcrumbs_home' => 1,
  );
  $display = new panels_display();
  $display->layout = 'radix_sutro';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
      'sidebar' => NULL,
      'contentheader' => NULL,
      'contentcolumn1' => NULL,
      'contentcolumn2' => NULL,
      'contentfooter' => NULL,
      'header' => NULL,
      'column1' => NULL,
      'column2' => NULL,
      'footer' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Tiempo: registrar día no laborado';
  $display->uuid = '63114551-9bb2-49c3-9684-a70c9275f2f7';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-dc912ad4-b2af-46a7-acc5-91f3cfd071c2';
    $pane->panel = 'column1';
    $pane->type = 'entity_form_field';
    $pane->subtype = 'node:field_dnl_razon';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => '',
      'formatter' => '',
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'dc912ad4-b2af-46a7-acc5-91f3cfd071c2';
    $display->content['new-dc912ad4-b2af-46a7-acc5-91f3cfd071c2'] = $pane;
    $display->panels['column1'][0] = 'new-dc912ad4-b2af-46a7-acc5-91f3cfd071c2';
    $pane = new stdClass();
    $pane->pid = 'new-cd7c3524-4828-4762-a478-33ce202c2ce1';
    $pane->panel = 'column1';
    $pane->type = 'entity_form_field';
    $pane->subtype = 'node:field_dnl_fecha';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => '',
      'formatter' => '',
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'cd7c3524-4828-4762-a478-33ce202c2ce1';
    $display->content['new-cd7c3524-4828-4762-a478-33ce202c2ce1'] = $pane;
    $display->panels['column1'][1] = 'new-cd7c3524-4828-4762-a478-33ce202c2ce1';
    $pane = new stdClass();
    $pane->pid = 'new-daae310d-8acd-4c18-a8bb-5e9e57e8c0d1';
    $pane->panel = 'column1';
    $pane->type = 'entity_form_field';
    $pane->subtype = 'node:field_dnl_detalles';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => '',
      'formatter' => '',
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'daae310d-8acd-4c18-a8bb-5e9e57e8c0d1';
    $display->content['new-daae310d-8acd-4c18-a8bb-5e9e57e8c0d1'] = $pane;
    $display->panels['column1'][2] = 'new-daae310d-8acd-4c18-a8bb-5e9e57e8c0d1';
    $pane = new stdClass();
    $pane->pid = 'new-b1833a78-3faa-47e3-adb0-73e0a5d2f707';
    $pane->panel = 'column2';
    $pane->type = 'entity_form_field';
    $pane->subtype = 'node:group_dnl_usuario';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => '',
      'formatter' => '',
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'b1833a78-3faa-47e3-adb0-73e0a5d2f707';
    $display->content['new-b1833a78-3faa-47e3-adb0-73e0a5d2f707'] = $pane;
    $display->panels['column2'][0] = 'new-b1833a78-3faa-47e3-adb0-73e0a5d2f707';
    $pane = new stdClass();
    $pane->pid = 'new-70859a4d-efaf-4d97-a5d1-7b64a2b0fa36';
    $pane->panel = 'footer';
    $pane->type = 'form';
    $pane->subtype = 'form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '70859a4d-efaf-4d97-a5d1-7b64a2b0fa36';
    $display->content['new-70859a4d-efaf-4d97-a5d1-7b64a2b0fa36'] = $pane;
    $display->panels['footer'][0] = 'new-70859a4d-efaf-4d97-a5d1-7b64a2b0fa36';
    $pane = new stdClass();
    $pane->pid = 'new-f59c4fb9-2132-4e92-8fcb-14895f1304f0';
    $pane->panel = 'footer';
    $pane->type = 'node_form_buttons';
    $pane->subtype = 'node_form_buttons';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'f59c4fb9-2132-4e92-8fcb-14895f1304f0';
    $display->content['new-f59c4fb9-2132-4e92-8fcb-14895f1304f0'] = $pane;
    $display->panels['footer'][1] = 'new-f59c4fb9-2132-4e92-8fcb-14895f1304f0';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-f59c4fb9-2132-4e92-8fcb-14895f1304f0';
  $handler->conf['display'] = $display;
  $export['node_edit__tiempo-registrar-d-a-no-laborado-2'] = $handler;

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'node_edit__tiempo-registrar-horas-sin-men-tiempo';
  $handler->task = 'node_edit';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -29;
  $handler->conf = array(
    'title' => 'Tiempo: registrar horas - sin menú tiempo',
    'no_blocks' => 0,
    'pipeline' => 'ipe',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'tiempo-registrar-horas-sin-men-tiempo',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'node_type',
          'settings' => array(
            'type' => array(
              'tiempo' => 'tiempo',
            ),
          ),
          'context' => 'argument_node_edit_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'panels_breadcrumbs_state' => 1,
    'panels_breadcrumbs_titles' => 'Tiempo
Registrar horas',
    'panels_breadcrumbs_paths' => 'tiempo',
    'panels_breadcrumbs_home' => 1,
  );
  $display = new panels_display();
  $display->layout = 'radix_sutro';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'right' => NULL,
      'bottom' => NULL,
      'sidebar' => NULL,
      'contentheader' => NULL,
      'contentcolumn1' => NULL,
      'contentcolumn2' => NULL,
      'contentfooter' => NULL,
      'header' => NULL,
      'column1' => NULL,
      'column2' => NULL,
      'footer' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Tiempo: registrar horas';
  $display->uuid = '63114551-9bb2-49c3-9684-a70c9275f2f7';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-0150f7c7-e253-4772-934a-606dc6c52d31';
    $pane->panel = 'column1';
    $pane->type = 'entity_form_field';
    $pane->subtype = 'node:field_tiempo_horas';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => '',
      'formatter' => '',
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '0150f7c7-e253-4772-934a-606dc6c52d31';
    $display->content['new-0150f7c7-e253-4772-934a-606dc6c52d31'] = $pane;
    $display->panels['column1'][0] = 'new-0150f7c7-e253-4772-934a-606dc6c52d31';
    $pane = new stdClass();
    $pane->pid = 'new-84bcddd0-6e00-4213-86b1-de3304223b84';
    $pane->panel = 'column1';
    $pane->type = 'entity_form_field';
    $pane->subtype = 'node:field_tiempo_actividad';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => '',
      'formatter' => '',
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '84bcddd0-6e00-4213-86b1-de3304223b84';
    $display->content['new-84bcddd0-6e00-4213-86b1-de3304223b84'] = $pane;
    $display->panels['column1'][1] = 'new-84bcddd0-6e00-4213-86b1-de3304223b84';
    $pane = new stdClass();
    $pane->pid = 'new-c15abcb8-e6c2-47ec-9a82-6ba50e10a259';
    $pane->panel = 'column1';
    $pane->type = 'entity_form_field';
    $pane->subtype = 'node:field_tiempo_detalles';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => '',
      'formatter' => '',
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'c15abcb8-e6c2-47ec-9a82-6ba50e10a259';
    $display->content['new-c15abcb8-e6c2-47ec-9a82-6ba50e10a259'] = $pane;
    $display->panels['column1'][2] = 'new-c15abcb8-e6c2-47ec-9a82-6ba50e10a259';
    $pane = new stdClass();
    $pane->pid = 'new-be15166d-5293-4bce-b0b3-b2a6703da431';
    $pane->panel = 'column2';
    $pane->type = 'entity_form_field';
    $pane->subtype = 'node:field_tiempo_fecha';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => '',
      'formatter' => '',
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'be15166d-5293-4bce-b0b3-b2a6703da431';
    $display->content['new-be15166d-5293-4bce-b0b3-b2a6703da431'] = $pane;
    $display->panels['column2'][0] = 'new-be15166d-5293-4bce-b0b3-b2a6703da431';
    $pane = new stdClass();
    $pane->pid = 'new-062fd9ad-2211-4aa1-b014-88d3df6aa359';
    $pane->panel = 'column2';
    $pane->type = 'entity_form_field';
    $pane->subtype = 'node:group_tiempo_usuario';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'label' => '',
      'formatter' => '',
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '062fd9ad-2211-4aa1-b014-88d3df6aa359';
    $display->content['new-062fd9ad-2211-4aa1-b014-88d3df6aa359'] = $pane;
    $display->panels['column2'][1] = 'new-062fd9ad-2211-4aa1-b014-88d3df6aa359';
    $pane = new stdClass();
    $pane->pid = 'new-6e55e0e0-b570-4412-b966-16c12b884efd';
    $pane->panel = 'footer';
    $pane->type = 'form';
    $pane->subtype = 'form';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '6e55e0e0-b570-4412-b966-16c12b884efd';
    $display->content['new-6e55e0e0-b570-4412-b966-16c12b884efd'] = $pane;
    $display->panels['footer'][0] = 'new-6e55e0e0-b570-4412-b966-16c12b884efd';
    $pane = new stdClass();
    $pane->pid = 'new-f59c4fb9-2132-4e92-8fcb-14895f1304f0';
    $pane->panel = 'footer';
    $pane->type = 'node_form_buttons';
    $pane->subtype = 'node_form_buttons';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => 'argument_node_edit_1',
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = 'f59c4fb9-2132-4e92-8fcb-14895f1304f0';
    $display->content['new-f59c4fb9-2132-4e92-8fcb-14895f1304f0'] = $pane;
    $display->panels['footer'][1] = 'new-f59c4fb9-2132-4e92-8fcb-14895f1304f0';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-f59c4fb9-2132-4e92-8fcb-14895f1304f0';
  $handler->conf['display'] = $display;
  $export['node_edit__tiempo-registrar-horas-sin-men-tiempo'] = $handler;

  return $export;
}

/**
 * Implements hook_default_page_manager_pages().
 */
function feature_tiempo_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'tiempo';
  $page->task = 'page';
  $page->admin_title = 'Tiempo';
  $page->admin_description = 'Páneles de la herramienta de registro de tiempo';
  $page->path = 'tiempo/!operacion/!id';
  $page->access = array(
    'plugins' => array(
      1 => array(
        'name' => 'role',
        'settings' => array(
          'rids' => array(
            0 => 3,
            1 => 4,
            2 => 5,
          ),
        ),
        'context' => 'logged-in-user',
        'not' => FALSE,
      ),
    ),
    'logic' => 'or',
  );
  $page->menu = array();
  $page->arguments = array(
    'operacion' => array(
      'id' => 1,
      'identifier' => 'Operación',
      'name' => 'string',
      'settings' => array(
        'use_tail' => 0,
      ),
    ),
    'id' => array(
      'id' => 1,
      'identifier' => 'ID de usuario',
      'name' => 'entity_id:user',
      'settings' => array(),
    ),
  );
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_tiempo__panel';
  $handler->task = 'page';
  $handler->subtask = 'tiempo';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Inicio',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '0',
          ),
          'context' => 'argument_string_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'panels_breadcrumbs_state' => 1,
    'panels_breadcrumbs_titles' => 'Tiempo',
    'panels_breadcrumbs_paths' => '',
    'panels_breadcrumbs_home' => 1,
  );
  $display = new panels_display();
  $display->layout = 'radix_sutro';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
      'header' => NULL,
      'column1' => NULL,
      'column2' => NULL,
      'footer' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Tiempo';
  $display->uuid = 'd83dc8a3-4115-472e-a6ba-669538e51996';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-6ce31d8b-e1c3-4988-bba4-7dc4ac0b5617';
    $pane->panel = 'header';
    $pane->type = 'node';
    $pane->subtype = 'node';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'nid' => '1',
      'links' => 0,
      'leave_node_title' => 0,
      'identifier' => '',
      'build_mode' => 'full',
      'link_node_title' => 0,
      'override_title' => 0,
      'override_title_text' => '',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '6ce31d8b-e1c3-4988-bba4-7dc4ac0b5617';
    $display->content['new-6ce31d8b-e1c3-4988-bba4-7dc4ac0b5617'] = $pane;
    $display->panels['header'][0] = 'new-6ce31d8b-e1c3-4988-bba4-7dc4ac0b5617';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_tiempo__panel_context_8de75d76-8548-4590-897d-7ccb77f57723';
  $handler->task = 'page';
  $handler->subtask = 'tiempo';
  $handler->handler = 'panel_context';
  $handler->weight = 1;
  $handler->conf = array(
    'title' => 'Mis horas',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => '',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'string_equal',
          'settings' => array(
            'operator' => '=',
            'value' => 'mis-horas',
            'case' => 0,
          ),
          'context' => 'argument_string_1',
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '0',
          ),
          'context' => 'argument_entity_id:user_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'panels_breadcrumbs_state' => 1,
    'panels_breadcrumbs_titles' => 'Tiempo
Mis registros',
    'panels_breadcrumbs_paths' => 'tiempo',
    'panels_breadcrumbs_home' => 1,
  );
  $display = new panels_display();
  $display->layout = 'radix_sutro';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'header' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
      'footer' => NULL,
      'column1' => NULL,
      'column2' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Tiempo: mis horas';
  $display->uuid = '182f94e8-8334-43ed-84a1-a5197dfd705c';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-d845a365-3d71-498b-9bb7-cbbf4c23dea4';
    $pane->panel = 'header';
    $pane->type = 'views_panes';
    $pane->subtype = 'tiempo-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'more_link' => 0,
      'use_pager' => 0,
      'pager_id' => '0',
      'items_per_page' => '0',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd845a365-3d71-498b-9bb7-cbbf4c23dea4';
    $display->content['new-d845a365-3d71-498b-9bb7-cbbf4c23dea4'] = $pane;
    $display->panels['header'][0] = 'new-d845a365-3d71-498b-9bb7-cbbf4c23dea4';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_tiempo__mis-d-as-no-laborados';
  $handler->task = 'page';
  $handler->subtask = 'tiempo';
  $handler->handler = 'panel_context';
  $handler->weight = 2;
  $handler->conf = array(
    'title' => 'Mis días no laborados',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'mis-d-as-no-laborados',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'string_equal',
          'settings' => array(
            'operator' => '=',
            'value' => 'mis-dias-no-laborados',
            'case' => 0,
          ),
          'context' => 'argument_string_1',
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '0',
          ),
          'context' => 'argument_entity_id:user_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'panels_breadcrumbs_state' => 1,
    'panels_breadcrumbs_titles' => 'Tiempo
Mis registros',
    'panels_breadcrumbs_paths' => 'tiempo',
    'panels_breadcrumbs_home' => 1,
  );
  $display = new panels_display();
  $display->layout = 'radix_sutro';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'header' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
      'footer' => NULL,
      'column1' => NULL,
      'column2' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Tiempo: mis días no laborados';
  $display->uuid = '182f94e8-8334-43ed-84a1-a5197dfd705c';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-388f7151-7135-4fb7-a16a-64c598e2b26a';
    $pane->panel = 'header';
    $pane->type = 'views_panes';
    $pane->subtype = 'tiempo-panel_pane_2';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'more_link' => 0,
      'use_pager' => 0,
      'pager_id' => '0',
      'items_per_page' => '0',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '388f7151-7135-4fb7-a16a-64c598e2b26a';
    $display->content['new-388f7151-7135-4fb7-a16a-64c598e2b26a'] = $pane;
    $display->panels['header'][0] = 'new-388f7151-7135-4fb7-a16a-64c598e2b26a';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_tiempo__horas-ajenas';
  $handler->task = 'page';
  $handler->subtask = 'tiempo';
  $handler->handler = 'panel_context';
  $handler->weight = 3;
  $handler->conf = array(
    'title' => 'Horas ajenas',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'horas-ajenas',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'string_equal',
          'settings' => array(
            'operator' => '=',
            'value' => 'horas',
            'case' => 0,
          ),
          'context' => 'argument_string_1',
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '1',
          ),
          'context' => 'argument_entity_id:user_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'panels_breadcrumbs_state' => 1,
    'panels_breadcrumbs_titles' => 'Tiempo
Horas',
    'panels_breadcrumbs_paths' => 'tiempo',
    'panels_breadcrumbs_home' => 1,
  );
  $display = new panels_display();
  $display->layout = 'radix_sutro';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'header' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
      'footer' => NULL,
      'column1' => NULL,
      'column2' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Registros de horas';
  $display->uuid = '182f94e8-8334-43ed-84a1-a5197dfd705c';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-d845a365-3d71-498b-9bb7-cbbf4c23dea4';
    $pane->panel = 'header';
    $pane->type = 'views_panes';
    $pane->subtype = 'tiempo-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'more_link' => 0,
      'use_pager' => 0,
      'pager_id' => '0',
      'items_per_page' => '0',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'd845a365-3d71-498b-9bb7-cbbf4c23dea4';
    $display->content['new-d845a365-3d71-498b-9bb7-cbbf4c23dea4'] = $pane;
    $display->panels['header'][0] = 'new-d845a365-3d71-498b-9bb7-cbbf4c23dea4';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_tiempo__d-as-no-laborados-ajenos';
  $handler->task = 'page';
  $handler->subtask = 'tiempo';
  $handler->handler = 'panel_context';
  $handler->weight = 4;
  $handler->conf = array(
    'title' => 'Días no laborados ajenos',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'd-as-no-laborados-ajenos',
    'access' => array(
      'plugins' => array(
        0 => array(
          'name' => 'string_equal',
          'settings' => array(
            'operator' => '=',
            'value' => 'dias-no-laborados',
            'case' => 0,
          ),
          'context' => 'argument_string_1',
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'context_exists',
          'settings' => array(
            'exists' => '1',
          ),
          'context' => 'argument_entity_id:user_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    ),
    'panels_breadcrumbs_state' => 1,
    'panels_breadcrumbs_titles' => 'Tiempo
Mis registros',
    'panels_breadcrumbs_paths' => 'tiempo',
    'panels_breadcrumbs_home' => 1,
  );
  $display = new panels_display();
  $display->layout = 'radix_sutro';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'header' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
      'footer' => NULL,
      'column1' => NULL,
      'column2' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Días no laborados';
  $display->uuid = '182f94e8-8334-43ed-84a1-a5197dfd705c';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-388f7151-7135-4fb7-a16a-64c598e2b26a';
    $pane->panel = 'header';
    $pane->type = 'views_panes';
    $pane->subtype = 'tiempo-panel_pane_2';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'more_link' => 0,
      'use_pager' => 0,
      'pager_id' => '0',
      'items_per_page' => '0',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '388f7151-7135-4fb7-a16a-64c598e2b26a';
    $display->content['new-388f7151-7135-4fb7-a16a-64c598e2b26a'] = $pane;
    $display->panels['header'][0] = 'new-388f7151-7135-4fb7-a16a-64c598e2b26a';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['tiempo'] = $page;

  return $pages;

}
