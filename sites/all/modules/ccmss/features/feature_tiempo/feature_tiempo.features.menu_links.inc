<?php
/**
 * @file
 * feature_tiempo.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function feature_tiempo_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-tiempo_catlogo-de-actividades:admin/structure/taxonomy/actividades.
  $menu_links['menu-tiempo_catlogo-de-actividades:admin/structure/taxonomy/actividades'] = array(
    'menu_name' => 'menu-tiempo',
    'link_path' => 'admin/structure/taxonomy/actividades',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Catálogo de actividades',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'menu-tiempo_catlogo-de-actividades:admin/structure/taxonomy/actividades',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-tiempo_configuracin:<nolink>',
  );
  // Exported menu link: menu-tiempo_configuracin:<nolink>.
  $menu_links['menu-tiempo_configuracin:<nolink>'] = array(
    'menu_name' => 'menu-tiempo',
    'link_path' => '<nolink>',
    'router_path' => '<nolink>',
    'link_title' => 'Configuración',
    'options' => array(
      'attributes' => array(
        'class' => array(
          0 => 'configuracion',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-tiempo_configuracin:<nolink>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-tiempo_hoja-de-tiempo:tiempo/hoja-de-tiempo.
  $menu_links['menu-tiempo_hoja-de-tiempo:tiempo/hoja-de-tiempo'] = array(
    'menu_name' => 'menu-tiempo',
    'link_path' => 'tiempo/hoja-de-tiempo',
    'router_path' => 'tiempo/hoja-de-tiempo',
    'link_title' => 'Hoja de tiempo',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-tiempo_hoja-de-tiempo:tiempo/hoja-de-tiempo',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'parent_identifier' => 'menu-tiempo_reportes:<nolink>',
  );
  // Exported menu link: menu-tiempo_inicio:tiempo.
  $menu_links['menu-tiempo_inicio:tiempo'] = array(
    'menu_name' => 'menu-tiempo',
    'link_path' => 'tiempo',
    'router_path' => 'tiempo',
    'link_title' => 'Inicio',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'menu-tiempo_inicio:tiempo',
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-tiempo_mis-das-no-laborados:tiempo/mis-dias-no-laborados.
  $menu_links['menu-tiempo_mis-das-no-laborados:tiempo/mis-dias-no-laborados'] = array(
    'menu_name' => 'menu-tiempo',
    'link_path' => 'tiempo/mis-dias-no-laborados',
    'router_path' => 'tiempo',
    'link_title' => 'Mis días no laborados',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-tiempo_mis-das-no-laborados:tiempo/mis-dias-no-laborados',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-tiempo_reportes:<nolink>',
  );
  // Exported menu link: menu-tiempo_mis-horas:tiempo/mis-horas.
  $menu_links['menu-tiempo_mis-horas:tiempo/mis-horas'] = array(
    'menu_name' => 'menu-tiempo',
    'link_path' => 'tiempo/mis-horas',
    'router_path' => 'tiempo',
    'link_title' => 'Mis horas',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-tiempo_mis-horas:tiempo/mis-horas',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-tiempo_reportes:<nolink>',
  );
  // Exported menu link: menu-tiempo_registrar-da-no-laborado:node/add/dia-no-laborado.
  $menu_links['menu-tiempo_registrar-da-no-laborado:node/add/dia-no-laborado'] = array(
    'menu_name' => 'menu-tiempo',
    'link_path' => 'node/add/dia-no-laborado',
    'router_path' => 'node/add/dia-no-laborado',
    'link_title' => 'Registrar día no laborado',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'menu-tiempo_registrar-da-no-laborado:node/add/dia-no-laborado',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-tiempo_registro:<nolink>',
  );
  // Exported menu link: menu-tiempo_registrar-tiempo:node/add/tiempo.
  $menu_links['menu-tiempo_registrar-tiempo:node/add/tiempo'] = array(
    'menu_name' => 'menu-tiempo',
    'link_path' => 'node/add/tiempo',
    'router_path' => 'node/add/tiempo',
    'link_title' => 'Registrar tiempo',
    'options' => array(
      'attributes' => array(),
      'identifier' => 'menu-tiempo_registrar-tiempo:node/add/tiempo',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-tiempo_registro:<nolink>',
  );
  // Exported menu link: menu-tiempo_registro:<nolink>.
  $menu_links['menu-tiempo_registro:<nolink>'] = array(
    'menu_name' => 'menu-tiempo',
    'link_path' => '<nolink>',
    'router_path' => '<nolink>',
    'link_title' => 'Registro',
    'options' => array(
      'attributes' => array(
        'class' => array(
          0 => 'registro',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-tiempo_registro:<nolink>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-tiempo_registros-de-das-no-laborados:tiempo/reporte-dias-no-laborados.
  $menu_links['menu-tiempo_registros-de-das-no-laborados:tiempo/reporte-dias-no-laborados'] = array(
    'menu_name' => 'menu-tiempo',
    'link_path' => 'tiempo/reporte-dias-no-laborados',
    'router_path' => 'tiempo/reporte-dias-no-laborados',
    'link_title' => 'Registros de días no laborados',
    'options' => array(
      'identifier' => 'menu-tiempo_registros-de-das-no-laborados:tiempo/reporte-dias-no-laborados',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'menu-tiempo_reportes:<nolink>',
  );
  // Exported menu link: menu-tiempo_registros-de-horas:tiempo/reporte-horas.
  $menu_links['menu-tiempo_registros-de-horas:tiempo/reporte-horas'] = array(
    'menu_name' => 'menu-tiempo',
    'link_path' => 'tiempo/reporte-horas',
    'router_path' => 'tiempo/reporte-horas',
    'link_title' => 'Registros de horas',
    'options' => array(
      'identifier' => 'menu-tiempo_registros-de-horas:tiempo/reporte-horas',
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'menu-tiempo_reportes:<nolink>',
  );
  // Exported menu link: menu-tiempo_reportes:<nolink>.
  $menu_links['menu-tiempo_reportes:<nolink>'] = array(
    'menu_name' => 'menu-tiempo',
    'link_path' => '<nolink>',
    'router_path' => '<nolink>',
    'link_title' => 'Reportes',
    'options' => array(
      'attributes' => array(
        'class' => array(
          0 => 'reportes',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-tiempo_reportes:<nolink>',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -48,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Catálogo de actividades');
  t('Configuración');
  t('Hoja de tiempo');
  t('Inicio');
  t('Mis días no laborados');
  t('Mis horas');
  t('Registrar día no laborado');
  t('Registrar tiempo');
  t('Registro');
  t('Registros de días no laborados');
  t('Registros de horas');
  t('Reportes');

  return $menu_links;
}
