<?php

function ccmss_theme_preprocess_page(&$vars) {
  if (module_exists('color')) {
    _color_page_alter($vars);
  }
}

function ccmss_theme_preprocess_html(&$vars) {
  global $user;
  foreach($user->roles as $role) {
    $vars['classes_array'][] = 'role-' . str_replace(' ', '-', $role);
  }
}
